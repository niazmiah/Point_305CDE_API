//user array
var user = {username: " ", 
            name: " ",
            age: 0,
            info1: " ",
            info2: " ",
            email: " ", 
            password: " ", 
            }, id, username, name, age, info1, info2, email, password, userarray = [];



function User(id, username, name, email, password, age) {
    'use strict';
    this.id = id;
    this.username = username;
    this.name = name;
    this.age = age;
    this.info1 = info1;
    this.info2 = info2;
    this.email = email;
    this.password = password;

}

function get(x){
    return document.getElementById(x); // function retrieves data from html page. 
}

function processP1() {
'use strict';
    id = userarray.length + 1
    username = document.getElementById("username").value;
    name = document.getElementById("name").value;
    age = document.getElementById("age").value;
    email = document.getElementById("email").value;
    password = document.getElementById("password").value;
    
    //Checking to make sure all fields are correctly filled.
       var x = document.forms["myForm"]["username"].value;
    if (x==null || x=="") {
        alert("Username must be filled out");
        return false;
    }
    var x = document.forms["myForm"]["name"].value;
    if (x==null || x=="") {
        alert("Name must be filled out");
        return false;
    }
    var x = document.forms["myForm"]["email"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Not a valid e-mail address");
        return false;
    }
    var x = document.forms["myForm"]["password"].value;
    if (x==null || x=="") {
        alert("Password must be filled out");
        return false;
    }
    var x = document.forms["myForm"]["age"].value;
    if (x==null || x=="") {
        alert("Age must be filled out");
        return false;
    }
    
    //Changing styling to part 2
        get("p1").style.display = "none";
		get("p2").style.display = "block";
		get("progressBar").value = 50;
		get("status").innerHTML = "Part 2 of 3";
    
}
    
function processP2() {
    'use strict';
    info1 = document.getElementById("info1").value;
    
    //Checking to make sure the field is correctly filled.
       var x = document.forms["myForm"]["info1"].value;
    if (x==null || x=="") {
        alert("Personal description must be filled out");
        return false;
    }
    
        //Changing styling to part 3
    	get("p2").style.display = "none";
		get("p3").style.display = "block";
		get("progressBar").value = 100;
		get("status").innerHTML = "Part 3 of 3";
}

function processP3() {
    'use strict';
    info2 = document.getElementById("info2").value;
    
    //Checking to make sure the field is correctly filled.
         var x = document.forms["myForm"]["info2"].value;
    if (x==null || x=="") {
        alert("Personal interests must be filled out");
        return false;
    }
    
    userarray[userarray.length] = new User(id, username, name, info1, info2, email, password, parseInt(age));
    
    

    
    var object = {}
    object["&username="] = username;
    object["&name="] = name;
    object["&age="] = age;
    object["&info1="] = info1;
    object["&info2="] = info2;
    object["&email="] = email;
    object["&password="] = password;
    
    var jsonObject = JSON.stringify(object); 
    var jsonTrimmed = jsonObject.replace(/:/g,'',/}/,/,/);
    
    
    
    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
        }
    }
 
     //Routing to the server
    xmlhttp.open("GET","http://127.0.0.1:8080/pointapi/users/register",true);
    xmlhttp.open("POST","http://127.0.0.1:8080/pointapi/users/register",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send(jsonTrimmed);
    
    alert("User added");
   
 //   xmlhttp.send("username=username&name=name&age=age&info=info&email=email&password=password" );  
}

window.onload = validateForm;