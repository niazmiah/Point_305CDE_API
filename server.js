// Node packages required
var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var User     = require('./user.js'); //User model
var mongoose   = require('mongoose');

//Connecting to database
mongoose.connect('mongodb://niaz:niaz@ds047581.mongolab.com:47581/phoenixinnovation');

//bodyParser to get POST json data
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Setup port
var port = process.env.PORT || 8080;
var router = express.Router(); //Express router instance

//Logs 'Handling request' everytime a request executed
router.use(function(req, res, next) {
    console.log('Handling request');
    next();
});

//GET - route to test if API is functioning
router.get('/', function(req, res) {
    res.json({ message: 'API test message' });
});


//POST - route to register a new user
router.route('/users/register')
//User registration
    .post(function(req, res) {

        var user = new User();
        user.username = req.body.username;
        user.name = req.body.name;
        user.age = req.body.age;
        user.email = req.body.email;
        user.password = req.body.password;
        user.info1 = req.body.info1;
        user.info2 = req.body.info2;
        user.goldPoints = 0;
        user.silverPoints = 10;
        //Save user and check for errors
        user.save(function(err) {
            if (!err) {
                return res.send(201, {message: 'User has been added.'});
            } else {
                console.log(err);
                return res.send(404, {message: 'User has not been added.' });
            }
        });

    });
//GET - retrieves the value of the silver and gold coins based on the user id being sent as a URL Parameter.
router.route('/coins/:user_id')
    .get(function(req, res, next) {
    User.findById(req.params.user_id, function(err, user){
        if (!err) {
                return res.json(201, {silver: user.silverPoints, gold: user.goldPoints });
            } else {
                console.log(err);
                return res.send(404, { error: "Could not find silver points" });
            }
        
    });
});

//PUT - changes the value of the silver points based on 2 body parameters and the userid, "silverPoints", "pointChange". silverPoint value can be retrieved with the get above. pointChange can be parsed as a positive or negative number eg. -2 or 2
router.route('/coins/silver')
    .put(function(req, res, next) {
    var pointsUpdate = new User({
        silverPoints: parseInt(req.body.silverPoints) + parseInt(req.body.pointChange)
        
    });
    var upsertData = pointsUpdate.toObject();
    
    delete upsertData._id;      

    return User.update({ 
        _id: req.body.user_id
    },upsertData, {upsert: true}, 
                       function(err) {
        if (!err) {
            return res.send(201, "Points updated");
        } else {
            console.log(err);
        return res.send(404, { error: "Points where not updated" });
          }
    });
});
//PUT - changes the value of the silver points based on 2 body parameters and the userid, "goldPoints", "pointChange". goldPoint value can be retrieved with the get above. pointChange can be parsed as a positive or negative number eg. -2 or 2
router.route('/coins/gold')
    .put(function(req, res, next) {
    var pointsUpdate = new User({
        goldPoints: parseInt(req.body.goldPoints) + parseInt(req.body.pointChange)
        
    });
    var upsertData = pointsUpdate.toObject();
    delete upsertData._id;      

    return User.update({ 
        _id: req.body.user_id
    },upsertData, {upsert: true}, 
                       function(err) {
        if (!err) {
            return res.send(201, "Points updated");
        } else {
            console.log(err);
        return res.send(404, { error: "Points where not updated" });
          }
    });
});

//GET - route to get all users in the database
router.route('/users')
    .get(function(req, res) {
        User.find(function(err, users) {
            if (err)
                res.send(err);
            res.json(200, users);
        });
    });


//Routes that end with users/user_id
router.route('/users/:user_id')
    //GET - route to retrieve user with a specific ID
    .get(function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            //Check for errors
            if (err)
                return res.send(404, { error: "User was not found." });
            res.json(200, user);
        });
    })

    //DELETE - route to delete a user with a specific ID
    .delete(function(req, res) {
        User.remove({
            _id: req.params.user_id
        }, 
            //Check for errors
            function(err, user) {
            if (!err) {
                return res.send(204, { message: "User has been deleted."});
            } else {
                console.log(err);
                return res.send(404, { error: "User was not deleted." });
            }
        });
    })

    //PUT - route to update a user with a specific ID
    .put(function(req, res, next) {
    var person = new User({
        name: req.body.name,
        age: req.body.age,
        email: req.body.email,
        password: req.body.password,
        info1: req.body.info1,
        info2: req.body.info2,
    });

    var upsertData = person.toObject();
    delete upsertData._id;      

    return User.update({ 
        _id: req.params.user_id 
    },upsertData, {upsert: true}, 
        //Check for errors               
        function(err) {
        if (!err) {
            return res.send(201, "User details updated.");
        } else {
            console.log(err);
        return res.send(404, { error: "User details was not updated." });
          }
    });
});

//Routes ending with /users/username    
router.route('/users/username/:username')
    //GET - route to get a user with a specific username
    .get(function(req, res) {
        User.findOne({'username' : req.params.username}, function(err, user) {
            if (err)
                return res.send(404, { error: "User with specified username was not found." });
            res.json(200, user);
        });
    })

    //DELETE - route to delete a user with a specific username
    .delete(function(req, res) {
        User.remove({
            username: req.params.username
        }, function(err, user) {
            if (!err) {
                return res.send(204, { message: "User has been deleted."});
            } else {
                console.log(err);
                return res.send(404, { error: "User was not deleted." });
            }
        });
    });

//Routes ending with age
router.route('/users/age/:age')
    //GET - route to get all users with a specific age
    .get(function(req, res) {
        User.find({'age' : req.params.age}, function(err, user) {
            if (err)
                return res.send(404, { error: "Users with specified age was not found." });
            res.json(200, user);
        });
    });
// Login

//router.route('/users/login')
//    .get(function(req, res) {
//        User.find({
//

//Routes ending with email
router.route('/users/email/:email')
    //GET - gets a user with a specific email
    .get(function(req, res) {
        User.find({'email' : req.params.email}, function(err, user) {
            if (err)
                return res.send(404, { error: "Users with specified email was not found." });
            res.json(200, user);
        });
    })

    //DELETE - route to delete a user with a specific email
    .delete(function(req, res) {
        User.remove({
            email: req.params.email
        }, function(err, user) {
            if (!err) {
                return res.send(204, { message: "User has been deleted."});
            } else {
                console.log(err);
                return res.send(404, { error: "User was not deleted." });
            }
        });
    });

//Register route - routes are added to /pointAPI/....
app.use('/pointAPI', router);

app.listen(port);
console.log('incoming request being handled');