var mongoose = require('mongoose');
var Schema = mongoose.Schema;
   
var UserSchema   = new Schema({
    username: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: true },
    name: { type: String, required: true },
    age: { type: Number, required: true },
    email: { type: String, required: true },
    info2: String,
    info1: { type: String, required: true },
    goldPoints: { type: Number, required: true },
    silverPoints: { type: Number, required: true }

});

module.exports = mongoose.model('User', UserSchema);