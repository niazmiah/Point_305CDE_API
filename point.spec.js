var frisby = require('frisby');
frisby.create('testing online')
    .get('http://127.0S.0.1:8080/pointapi')
    .expectStatus(200)
    .expectHeader('Content-Type', 'application/json')
    .expectJSONTypes({
        message: String
    })
    .expectJSON({
        "message": "API test message"
    })
.toss();

frisby.create('testing add user')
    .post('http://127.0.0.1:8080/pointapi/users/register', {
    username: "testuser",
    name: "test1",
    age: 22,
    email: "test@hotmail.com",
    password: "test",
    info1: "test",
    info2: "test2",
    goldPoints: 0,
    silverPoints: 10,
})
.expectStatus(201)
.expectJSON({ 
    message: 'User has been added.'
})
.toss();

frisby.create('testing add duplicate username user')
    .post('http://127.0.0.1:8080/pointapi/users/register', {
    username: "testuser",
    name: "test1",
    age: 22,
    email: "test@hotmail.com",
    password: "test",
    info1: "test",
    info2: "test2",
    goldPoints: 0,
    silverPoints: 10,
})
.expectStatus(404)
.expectJSON({ 
    message: 'User has not been added.'
})
.toss();


frisby.create('get user by username testuser')
    .get('http://127.0.0.1:8080/pointapi/users/username/testuser')
    .expectStatus(200)
    .expectJSONTypes({
     username: String,
     password: String,
     name: String,
     age: Number,
     email: String,
     info2: String,
     info1: String,
     goldPoints: Number,
     silverPoints: Number
})
.expectJSON({
    username: "testuser",
    name: "test1",
    age: 22,
    email: "test@hotmail.com",
    password: "test",
    info1: "test",
    info2: "test2",
    goldPoints: 0,
    silverPoints: 10,
})
.toss();

frisby.create('delete user by username testuser')
    .delete('http://127.0.0.1:8080/pointapi/users/username/testuser')
.expectStatus(204)
.expectJSON({ 
    message: 'User has been deleted.'
})
.toss();

frisby.create('delete user by email test@hotmail.com')
    .delete('http://127.0.0.1:8080/pointapi/users/email/test@hotmail.com')
.expectStatus(204)
.expectJSON({ 
    message: 'User has been deleted.'
})
.toss();

frisby.create('get user by email test@hotmail.com')
    .get('http://127.0.0.1:8080/pointapi/users/email/test@hotmail.com')
    .expectStatus(200)
    .expectJSONTypes({
     username: String,
     password: String,
     name: String,
     age: Number,
     email: String,
     info2: String,
     info1: String,
     goldPoints: Number,
     silverPoints: Number
})
.expectJSON({
    username: "testuser",
    name: "test1",
    age: 22,
    email: "test@hotmail.com",
    password: "test",
    info1: "test",
    info2: "test2",
    goldPoints: 0,
    silverPoints: 10,
})
.toss();
    
frisby.create('get user by age 22')
    .get('http://127.0.0.1:8080/pointapi/users/age/22')
    .expectStatus(200)
    .expectJSONTypes({
     username: String,
     password: String,
     name: String,
     age: Number,
     email: String,
     info2: String,
     info1: String,
     goldPoints: Number,
     silverPoints: Number
})
.expectJSON({
    username: "testuser",
    name: "test1",
    age: 22,
    email: "test@hotmail.com",
    password: "test",
    info1: "test",
    info2: "test2",
    goldPoints: 0,
    silverPoints: 10,
})
.toss();
    

        